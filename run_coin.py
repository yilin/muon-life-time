#!/usr/bin/env python
from argparse import ArgumentParser
from ROOT import TH1D,TCanvas,gROOT,TGraph,TLatex,gStyle,TF1,gDirectory,TPad,TLine,TFile
import array
gROOT.SetBatch(True)
gROOT.Reset()

print("Loading scripts (This will take some time)")
gROOT.LoadMacro('./script/CoinTestmain.C') # load class and function in ROOT 
#gROOT.LoadMacro('./script/CalPedestal.C')

from ROOT import CoinTest # import class CoinTest from ROOT

import math
import sys,os
sys.path.append('lib/')

from plotter import * #PlotMuonAndFit,PlotCoincidenceFive,PlotPlain,PlotAChannel

import atlas_style

import time,datetime

def ReadPedestal(P_name):
    peds = {}
    with open(P_name,'r') as fp:
        for line in fp.readlines():
            if "#" in line: continue
            line = line.replace('\n','').split(':')
            if len(line)==2:
                    peds[line[0]] = float(line[1])
    return peds

def ReadInput(isList,m_file):
    if not isList: return [["",m_file]]
    files = []
    with open(List,'r') as fp:
        m_path = ""
        for line in fp.readlines():
            if "#" in line : continue
            line = line.replace('\n','').replace('\t','')
            line_token = line.split()
            if len(line_token)>0 and line_token[0] == "Path":
                m_path = line_token[2]
            else:
                files.append([m_path,line.replace(' ','')])
    return files


def TestCoincidence(Input,List,Output,Delay,P_name,Life_time,hardware,isOutRoot):
    # Read a root file or a list of root files
    islist = os.path.isfile(List)
    files =  []
    if islist:
        files = ReadInput(islist, List)
    else:
        files = ReadInput(islist,Input)
    
    peds = ReadPedestal(P_name) # get pedestal level

    Muon_list = {}
    start_comp = {}
    start_comp["AB"] = [] 
    stop_comp = {}
    stop_comp["AB"] = [] 
    fout = None
    if isOutRoot:
        print ("Create TFile : ",format(os.path.join(Output,"output.root")))
        fout = TFile(os.path.join(Output,"output.root"),"RECREATE")

    if Life_time:
        Muon_list["Channel"] = "0,1,2"
        Muon_list["Muon_events"] = []
        Muon_list["hardware_failed_events"] = []

    print("Checking all files!")
    for full_path in  files:
        file_path = os.path.join(full_path[0],full_path[1])
        if not os.path.isfile(file_path):
            print ("{0} is not found!".format(file_path))
            return
    print ("{0} files are checked!".format(len(files)))

    #loop all files 
    counter = 0
    for full_path in files:
        file_path = os.path.join(full_path[0],full_path[1])
        ct = CoinTest(file_path,P_name) # see ./script/CoinTest.h for details
        nEntries = ct.GetEntries()
        entry = 0
        while entry < nEntries: # loop the TTree
            #if entry >0: break
            counter+=1
            ct.ProcessEntry(entry)
            ADCs = {}
            ADCs[0] = ct.GetA()
            ADCs[1] = ct.GetB()
            ADCs[2] = ct.GetC()
            ADCs["p_0"] = peds[str(ct.GetChannelA())]
            ADCs["p_1"] = peds[str(ct.GetChannelB())]
            ADCs["p_2"] = peds[str(ct.GetChannelC())]
            if ct.GetSTART() and ct.GetSTARTSize()!=0:
                ADCs[3] = ct.GetSTART()
                ADCs["p_3"] = peds[str(ct.GetChannelSTART())]
            if ct.GetSTOP() and ct.GetSTOPSize()!=0:
                ADCs[4] = ct.GetSTOP()
                ADCs["p_3"] = peds[str(ct.GetChannelSTART())]
            entry+=1
            ADCs["Pass"] = ct.isPass() 
            ADCs["Project"] = ct.GetProject() 
            ADCs["FileName"] = ct.GetFileName() 
            if Life_time:
                if ct.isPass():
                    Muon_list["Muon_events"].append(ct.GetLife_time())
                if not ct.isHWPass():
                    Muon_list["hardware_failed_events"].append(ct.GetLife_time())
                if hardware:
                    if ct.HasSTART_comp():
                        start_comp["AB"].append(ct.GetSTART_comp())
                    if ct.HasSTOP_comp():
                        stop_comp["AB"].append(ct.GetSTART_comp())
            else:
                flags_dict = {}
                flags_dict["AB_C"] = ct.flags_AB_C;
                flags_dict["_AB_C"] = ct.flags__AB_C;
                PlotCoincidenceFive(
                                      ADCs,
                                      flags_dict,
                                      Output,
                                      fout,
                                      name = "flags_"+str(counter),
                                      Label_Off = True,
                                      #x_title = "Sampling points",
                                      #y_title = "ADC",
                                      #y_min = 0 if Histo["Min"]-2*(Histo["Max"]-Histo["Min"])<0 else Histo["Min"]-2*(Histo["Max"]-Histo["Min"]),
                                      #y_max = Histo["Max"]+2*(Histo["Max"]-Histo["Min"]),
                                      x_min = 0,
                                      x_max = 200,
                                      #x_max = 512,
                                      #y_min = 0 ,
                                      #y_max = 16000,
                                      width = 1200,
                                     )
            if isOutRoot and counter % 100 == 0: print("{0} is processed".format(counter))
    if Life_time:
        PlotMuonAndFit(
                     Muon_list,
                     hardware,
                     Output,
                     name = "Muon_life_time_012",
                     Label_Off = True,
                     x_title = "Decay time [#mus]",
                     y_title = "Events",
                     #y_min = -100,
                     #y_max = 100,
                     #y_min = Histo["Bias_Min"]-2*(Histo["Bias_Max"]-Histo["Bias_Min"]),
                     #y_max = Histo["Bias_Max"]+2*(Histo["Bias_Max"]-Histo["Bias_Min"]),
                     width = 1000,
                     )
        if hardware:
            PlotPlain(
                     start_comp["AB"],
                     Output,
                     name = "start_comp",
                     Label_Off = True,
                     x_title = "t_{hardware}-t_{analysis} [ns]",
                     y_title = "Events",
                     x_min = -200,
                     x_max = 200,
                     #y_min = Histo["Bias_Min"]-2*(Histo["Bias_Max"]-Histo["Bias_Min"]),
                     #y_max = Histo["Bias_Max"]+2*(Histo["Bias_Max"]-Histo["Bias_Min"]),
                     width = 800,
                     )
            PlotPlain(
                     stop_comp["AB"],
                     Output,
                     name = "stop_comp",
                     Label_Off = True,
                     x_title = "t_{hardware}-t_{analysis} [ns]",
                     y_title = "Events",
                     x_min = -200,
                     x_max = 200,
                     #y_min = Histo["Bias_Min"]-2*(Histo["Bias_Max"]-Histo["Bias_Min"]),
                     #y_max = Histo["Bias_Max"]+2*(Histo["Bias_Max"]-Histo["Bias_Min"]),
                     width = 800,
                     )
    
    if isOutRoot: 
        fout.Close()
        fout.Delete()

if __name__=="__main__":

    parser = ArgumentParser()
    parser.add_argument("-i", dest='Input',default="FADC.txt",help="The file of ADC output")
    parser.add_argument("-l", dest='List',default="",help="The list for the files of ADC output")
    parser.add_argument("--life-time", dest='Life_time',action="store_true",help="Peak up the event with muon decay")
    parser.add_argument("--hardware", dest='Hardware',action="store_true",help="Plot events missing in hardware")
    parser.add_argument("-o", dest='Output',default="./",help="The output directory")
    parser.add_argument("-n", dest='P_name',default="pedestal.txt",help="file name for the pedestal level")
    parser.add_argument("-d", dest='Delay',type=int,default=2,help="delay points for A and B (10ns per point)")
    parser.add_argument("-r", dest='isOutRoot',action="store_true",help="Out put the root file for each event")
    args = parser.parse_args()
    Input = args.Input
    List = args.List
    Output = args.Output
    P_name = args.P_name
    Life_time = args.Life_time
    hardware = args.Hardware
    Delay = args.Delay
    isOutRoot = args.isOutRoot

    stamp_a = datetime.timedelta(seconds=time.time())

    if not os.path.isdir(Output):
        os.makedirs(Output)

    TestCoincidence(Input,List,Output,Delay,P_name,Life_time,hardware,isOutRoot)

    stamp_b = datetime.timedelta(seconds=time.time())
    print("Process time : {0}".format(stamp_b-stamp_a))
