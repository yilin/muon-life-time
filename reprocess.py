#!/usr/bin/env python
from argparse import ArgumentParser
from ROOT import gROOT,TH1D
#gROOT.SetBatch(True)
#gROOT.Reset()

print("Loading scripts (This will take some time)")
gROOT.LoadMacro('./script/MyReProcess.C') # load MyReProcess in ROOT
gROOT.LoadMacro('./script/CalPedestal.C') # load MyReProcess in ROOT

from ROOT import MyReProcess,CalPedestal # import fuction from ROOT

import os,re

import time,datetime

def ReadList(List):
	tokens = []
	print ("Reading list".format(List))
	dirs = []
	with open(List,'r') as fp:
		m_path = ""
		for line in fp.readlines():
			if "#" in line : continue
			line = line.replace('\n','').replace('\t','')
			line_token = line.split()
			if len(line_token)>0 and line_token[0] == "Path":
				m_path = line_token[2]
			else:
				dirs.append([m_path,line.replace(' ','')])
	return dirs
  

def Run(Directory,List,OutDir,Config,Pedestal,isPG):
	dirs = []
	# Read a root file or a list of root files
	if len(List)!=0:
		dirs = ReadList(List)
	else:
		if len(Directory)==0: 
			print ("No Directory!")
			return
		else:
			dirs = [["",Directory]]
	project = ""
	for m_dir in dirs:
		full_path = os.path.join(m_dir[0],m_dir[1])
		# retrieve project name from the directory name
		if full_path[-1] == '/':
			project = full_path.split('/')[-2]
		else:
			project = full_path.split('/')[-1]
		# calculate pedestal levels or create a root file
		if Pedestal:
			print("Calculating pedestals")
			CalPedestal(full_path,project,OutDir)
		else:
			MyReProcess(full_path,project,OutDir,Config,isPG)

if __name__=="__main__":


	parser = ArgumentParser()
	parser.add_argument("-d", dest='Directory',default="",help="The directory of data")
	parser.add_argument("-l", dest='List',default="",help="The list for the files of ADC output")
	parser.add_argument("-o", dest='OutDir',default="root_output",help="The output directory")
	#parser.add_argument("-c", dest='Config',default="./config/channel_setting.config",help="The configuration of the projects")
	parser.add_argument("-c", dest='Config',default="",help="The configuration of the projects")
	parser.add_argument("-p", dest='Pedestal',action="store_true",help="Calculate pedestal level")
	parser.add_argument("--progress", dest='isPG',action="store_true",help="turn on the progress status")

	args = parser.parse_args()
	Directory = args.Directory
	OutDir = args.OutDir
	Config = args.Config
	Pedestal = args.Pedestal
	List = args.List
	isPG = args.isPG


	if not os.path.isdir(OutDir):
		os.makedirs(OutDir)

	stamp_a = datetime.timedelta(seconds=time.time())

	Run(Directory,List,OutDir,Config,Pedestal,isPG)

	stamp_b = datetime.timedelta(seconds=time.time())
	print("Process time  : {}".format(stamp_b-stamp_a))

