#!/usr/bin/env python
from argparse import ArgumentParser
from ROOT import gROOT
gROOT.SetBatch()

import math
import sys,os
sys.path.append('lib/')

import atlas_style
from plotter import *

import time,datetime

def SearchDir(m_dir):
    samples = []
    if not os.path.isdir(m_dir):
        return samples
    for mfile in os.listdir(m_dir): 
        if "Run_" in mfile and ".log" in mfile:
            samples.append([m_dir,mfile])
    
    return sorted(samples, key=lambda x : len(x[1]))

def ReadInput(isList,m_file):
    if not isList: return [["",m_file]]
    files = []
    with open(List,'r') as fp:
        m_path = ""
        for line in fp.readlines():
            if "#" in line : continue
            line = line.replace('\n','').replace('\t','')
            line_token = line.split()
            if len(line_token)>0 and line_token[0] == "Path":
                m_path = line_token[2]
            else:
                files.append([m_path,line.replace(' ','')])
    return files

def ReadData(m_file):
    data = []
    with open(m_file,'r') as fp:
        for line in fp.readlines():
            line = line.replace('\n','').replace('\t','')
            token = line.split()
            if len(token)!=5 : continue
            if token[0] !="DATA" and token[0]!="TDC":continue
            try:
                data.append([int(token[1]),int(token[2]),int(token[3]),int(token[4])])
            except:
                print("failed to parse \"{0}\"".format(line))
    return data

def Run_TDC(Input,List,InputDir,Output,start,stop):
    isdir = os.path.isdir(InputDir)
    files =  []
    if isdir:
        files = SearchDir(InputDir) # if input is a directory
    else:
        islist = os.path.isfile(List)
        if islist: 
            samples = ReadInput(islist, List)
            if len(samples)>0 and os.path.isdir(os.path.join(samples[0][0],samples[0][1])):
                # if input is a list of directories
                for sp in samples:
                    files_for_each = SearchDir(os.path.join(sp[0],sp[1]))
                    for each in files_for_each:
                        files.append(each)
            else:
                files=samples # if input is a list of files
        else:
            files = ReadInput(islist,Input) # if input is a file

    Muon_list = {}
    Muon_list["Muon_events"] = []
    # loop files
    for full_path in files:
        full_name = os.path.join(full_path[0],full_path[1])
        print("Reading {0}".format(full_name))
        measurements = ReadData(full_name)
        start_timestamp = 0
        for i,mea in enumerate(measurements):
            # start paring the START and STOP if START is detected and the next channel is stop channel
            # Also Event ID and Bunch must be matched
            if start_timestamp and mea[2] ==stop and mea[0] == measurements[i-1][0] and mea[1] == measurements[i-1][1]:
                muon_life_time = (mea[3]-start_timestamp)*0.000025
                Muon_list["Muon_events"].append(muon_life_time)
            # Whether it matched or not, reset flag for searching START
            start_timestamp = 0
            if mea[2] == start:
                start_timestamp = mea[3]
    #Muon_list["Channel"] = "8,12"
    PlotMuonAndFit(
            Muon_list,
            False,
            Output,
            Fit_range = [0.15,20.15],
            name = "TDC_muon",
            Label_Off = True,
            x_title = "Decay time [#mus]",
            y_title = "Events",
            width = 1000,
    )
    

if __name__=="__main__":

    parser = ArgumentParser()
    parser.add_argument("-i", dest='Input',default="data/Run_0804.log",help="The file of TDC output")
    parser.add_argument("-l", dest='List',default="",help="The list for the files of TDC output")
    parser.add_argument("-d", dest='InputDir',default="",help="The direcotry of TDC data")
    parser.add_argument("-o", dest='Output',default="./",help="The output directory")
    parser.add_argument("--start", dest='start',type=int, default=8,help="The input channel of the START signal")
    parser.add_argument("--stop", dest='stop',type=int, default=12,help="The input channel of the STOP signal")

    args = parser.parse_args()
    Input = args.Input
    List = args.List
    InputDir = args.InputDir
    Output = args.Output
    start = args.start
    stop = args.stop

    stamp_a = datetime.timedelta(seconds=time.time())

    if not os.path.isdir(Output):
        os.makedirs(Output)

    Run_TDC(Input,List,InputDir,Output,start,stop)

    stamp_b = datetime.timedelta(seconds=time.time())
    print("Process time : {0}".format(stamp_b-stamp_a))
