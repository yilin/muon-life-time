#!/bin/bash
if [ -d "${1}" ];then
    p=$(echo ${1} | sed 's/\// /g'| awk '{print $NF}')
    echo "Project Name : ${p}"
    echo "#########################################################################"
    echo ""
    echo "Organizing the data and Plotting the muon life time"
    echo ""
    echo "#########################################################################"
    if [ "${2}" ] && [ "${3}" ];then
        python TDC_muon.py -d ${1} -o results/${p}_TDC/ --start ${2} --stop ${3}
    else
        python TDC_muon.py -d ${1} -o results/${p}_TDC/
    fi
else
    echo "please use the name of directory as an input!"
fi
