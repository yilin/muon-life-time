# Analysis of muon life-time experiment
## Requirement
ROOT framework with PyROOT. https://root.cern.ch/
## Introduction
This package is used for the data analysis in the muon life-time experiment with CAEN V1724 8 channel digitizer (ADC) and CAEN V1290 16 channel time to digital converter (TDC). The following discripts experiment briefly and the basic structure of the package.
## Experimental Setup and Input Signals

|\==\==\==\==\==\==\==\==\==\==\==\==\==| &#8592; $`A`$\
&#8195;&#8195;&#8195;|------------------------------|\
&#8195;&#8195;&#8195;|&#8195;&#8195;&#8195;&#8195;&#8195;&#8195;&#8195;&#8195;&#8195;&#8195;&#8195;&#8195;| &#8592; $`B`$\
&#8195;&#8195;&#8195;|------------------------------|\
|==========================| &#8592; $`C`$

Three scintillators are setup as shown in schematics. The top one is defined as $`A`$. The middle and bottom ones are defined as $`B`$ and $`C`$, respectively. This experiment aims for the muons going through $`A`$, stopping in $`B`$, and decaying in $`B`$. The whole process of the muon decay event can be detected by a START signal and a STOP signal. A START signal is composed of $`AB\bar{C}`$ . A STOP signal is composed of $`\bar{A}B\bar{C}`$. $`A`$, $`B`$, $`C`$, START and STOP are key concepts used in the analysis.
## Raw Data Structure for ADC
The raw data are stored in the text files with names like Run_*.log. The asterisk presents the different numbers, for example, Run_1.log, Run_2.log, … and so on. Each sampling point of data is formatted into (`DATA` `event number` `ADC channel` `sampling point` `ADC value`) or (`ADC` `event number` `ADC channel` `sampling point` `ADC value`).
## Raw Data Structure for TDC 
The raw data are stored in the text files with names like Run_*.log. The asterisk presents the different numbers, for example, Run_1.log, Run_2.log, … and so on. Each sampling point of data is formatted into (`DATA` `event ID` `bunch ID` `TDC channel` `time stamp`) or (`TDC` `event ID` `bunch ID` `TDC channel` `time stamp`).
## Package Structure
The package is based on the ROOT framework. To use this package, please setup ROOT in the beginning. Three main macro are written in python language. Main data processes are written in C++. The following explain the information of directories. 
* `config` : The settings of the input signals are kept here. The details of the settings will be explained later.   
* `lib` : The plotting style and method.
* `script` : creation of TTree in root files and processing with TTree.
* `data` : where the raw data is stored.
## Run an Example
You can test the package by running an example.

`sh example_adc.sh`  for ADC

`sh example_tdc.sh`  for TDC
## Run Full Analysis
If you have data files in `./data/Run_0810/` and a config file `./config/channel_setting_0702.config`, run

`sh full_analysis_adc.sh ./data/Run_0810/ ./config/channel_setting_0702.config`  for ADC

or

`sh full_analysis_tdc.sh ./data/Run_0810/`  for TDC

The second argument for ADC can be ignored since it is set as default.

If you use channels different from the default settings (CH 8 for START, CH 12 for STOP) for TDC, for example, CH 6 for START and CH 10 for STOP, run

`sh full_analysis_tdc.sh ./data/Run_0810/ 6 10`
## Macros
* `reprocess.py` : converts the raw data files into root files and calculates the pedestal levels from the pedestal run.
* `run_basic.py` : plots whole sampling points of all events in each channel and the distribution of muon life time for data that provide START and STOP signals.
* `run_coin.py` : provides coincidence analysis and distribution of muon life time if $`A`$, $`B`$, $`C`$ are provided. Cross checks can be performed if START and STOP are also provided.
* `TDC_muon.py` : reads the raw data files and plot the distribution of muon life time
### Usage of macros for ADC

```mermaid
graph TD

CO[Configuration] --> B(reprocess.py)
A[ADC Raw Data]  --> B

B --> C[root file]

B --> P[pedestal levels]

C --> D(run_basic.py)

C --> E(run_coin.py)

D --> G[TGraph with all points]

D --> M[muon life time]

E --> |Coincidence analysis| M

E --> |Coincidence analysis| N[Pulse correlation]

P --> E
```
The flow chart shows the dependences between input/output files and macros. The raw data must be processed by `reprocess.py`, for example,
`python reprocess.py -d Run_0616_1 -c config/channel_setting.config`
The default output directory is `root_output`. The output root files are named with the same name of the directory. The options are as follows.
```
usage: reprocess.py [-h] [-d DIRECTORY] [-l LIST] [-o OUTDIR] [-c CONFIG] [-p] [--progress]

optional arguments:
  -h, --help    show this help message and exit
  -d DIRECTORY  The directory of data
  -l LIST       The list for the files of ADC output
  -o OUTDIR     The output directory
  -c CONFIG     The configuration of the projects
  -p            Calculate pedestal level
  --progress    turn on the progress status
  ```
From the root files, you can use the macros as follows.
* For the basic check of data in each channel, run

`python run_basic.py -i ./root_output/Run_0616_1.root -o ./results/Run_0616/ `
* For the distribution of muon life time with START and STOP signal, run

`python run_basic.py -i ./root_output/Run_0616_1.root -o ./results/Run_0616/ --life-time`
* For the analysis on $`A`$,$`B`$,$`C`$ signals and produce comparisons between input channels, run

`python run_coin.py -i ./root_output/Run_0616_1.root -o ./results/Run_0616/`

If you provide START and STOP signals as well, It can run more detailed checks.
* For muon life time , run

`python run_coin.py -i ./root_output/Run_0616_1.root -o ./results/Run_0616/ --life-time`

If you provide START and STOP signals as well, It can run more detailed checks.
### Usage of macros for TDC
For example, the CH 8 and CH12 are used as START and STOP signals, respectively.

* For a raw data file, run

`python TDC_muon.py -i ./data/Run_0810/Run_1.log -o ./results/Run_0810/ --start 8 --stop 12`
* For raw data files in `./data/Run_0810/`, run

`python TDC_muon.py -d ./data/Run_0810/ -o ./results/Run_0810/ --start 8 --stop 12`
* For raw data files in multiple directories, see next section for creating a list and run

`python TDC_muon.py -l ./lists/list_muon_tdc.txt -o ./results/Muon_TDC/ --start 8 --stop 12`

Note: All the `--start 8 --stop 12` can be ignored since they are default values.
### A List as a Input File
Those three macros provide a feature that you can use a list to put multiple inputs. The list must be formatted as follows.

Example 1:
```
Path = ./data/
Run_0616_1
Run_0617_1
```
or

Example 2:
```
Path = ./root_output/
Run_0616_1.root
Run_0616_1.root
```
For both examples, you can use `Path` to tell the common path of those inputs. Example 1 is for `reprocess.py`, and Example 2 is for both `run_basic.py` and `run_coin.py`
### Configuration of Input Signals for ADC
A configuration file is used as a input in `reprocess.py`.  As shown in the `config/channel_setting.config`, the $`A`$, $`B`$, $`C`$, START, and STOP can be assigned to ADC channels. The assignments will be stored in the branches of  TTree as `channel_x`.  The `run_basic.py` and `run_coin.py` take these assignments for the process of the calculations. So, be sure you have correct assignments on those channels. If you don't have any assignment, the configuration file is not needed, and you will find `channel_x` are all -1. The configuration of a channel is composed of a ADC channel on the left, a colon in the middle, and a signal on the right like `0:A`. The square brackets indicate `project name`, which is automatically created in `reprocess.py` by using your directory name. The default setting will be applied to those project, which are not specified in the configuration file.  
```
#For the cross check of hardware setup
#and coincidence analysis
[default]
0:A
1:B
2:C
3:START
4:STOP

[Run_0614_1_invertStartStop]
0:A
1:B
2:C
3:START
4:STOP
```
