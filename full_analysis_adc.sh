#!/bin/bash
config="config/channel_setting_0702.config"
if [ -d "${1}" ];then
    p=$(echo ${1} | sed 's/\// /g'| awk '{print $NF}')
    if [ -f "${2}" ];then
        config=${2}
    fi 
    echo "Project Name : ${p}"
    echo "#########################################################################"
    echo ""
    echo "Converting text files into root file"
    echo ""
    echo "#########################################################################"
    python reprocess.py -d ${1} -c ${config}
    echo "#########################################################################"
    echo ""
    echo "Plotting all channels"
    echo ""
    echo "#########################################################################"
    python run_basic.py -i root_output/${p}.root -o results/${p}_basic/
    echo "#########################################################################"
    echo ""
    echo "Plotting the muon life time from START and STOP"
    echo ""
    echo "#########################################################################"
    python run_basic.py -i root_output/${p}.root -o results/${p}_basic/ --life-time
    echo "#########################################################################"
    echo ""
    echo "Check correlation of A,B,C, START, and STOP"
    echo ""
    echo "#########################################################################"
    python run_coin.py -i root_output/${p}.root -o results/${p}_coin/
    echo "#########################################################################"
    echo ""
    echo "Plotting the muon life time from A,B,C"
    echo ""
    echo "#########################################################################"
    python run_coin.py -i root_output/${p}.root -o results/${p}_coin/ --life-time
    echo "#########################################################################"
    echo ""
    echo "Plotting the muon life time from A,B,C with correlation of START and STOP"
    echo ""
    echo "#########################################################################"
    python run_coin.py -i root_output/${p}.root -o results/${p}_coin_hardware/ --life-time --hardware
else
    echo "please use the name of directory as an input!"
fi
