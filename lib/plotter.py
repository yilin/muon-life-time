
from plot_base_p3 import *

from ROOT import TH1D,TCanvas,gROOT,TGraph,TLatex,gStyle,TF1,gDirectory,TPad,TLine,TFile

import os

class PlotMuonAndFit(PlotBase):
    def __init__(self, Muon_list,ishardware, Output, Fit_range = [], **kwargs):
        super(PlotMuonAndFit, self).__init__(**kwargs)

        #self.canvas.Divide(1,2)
        self.pad1 = TPad("pad1","pad1",0,0.3,1,1)
        self.pad2 = TPad("pad2","pad2",0,0,1,0.27)
        self.pad1.Draw()
        self.pad2.Draw()
        self.pad1.SetLeftMargin(0.13)
        self.pad2.SetLeftMargin(0.13)
        self.pad1.SetTopMargin(0.11)
        self.pad1.SetBottomMargin(0.03)
        self.pad2.SetTopMargin(0.1)
        self.pad2.SetBottomMargin(0.30)
        self.pad2.SetGridy(1)
        self.pad1.SetBorderSize(0)
        self.pad2.SetBorderSize(0)

        gStyle.SetOptFit(1111)

        self.pad1.cd()
        h_frame = TH1D(self.name,";"+self.x_title+";"+self.y_title,100,0.15,20.15)
        h_hardware = None
        if ishardware:
            h_hardware = TH1D(self.name+"hardware",";"+self.x_title+";"+self.y_title,100,0.15,20.15)
            h_hardware.SetLineColor(2)
            #h_hardware.SetLineWidth(0)
            h_hardware.SetFillColor(2)
            h_hardware.SetFillStyle(3335)
            h_frame.SetFillColor(1)
            h_frame.SetFillStyle(3353)
        #h_frame.SetStats(0)
        self.set_y_min(h_frame)
        self.set_y_max(h_frame)
        for one_muon in Muon_list["Muon_events"]:
            h_frame.Fill(one_muon)
        if ishardware:
            for one_muon in Muon_list["hardware_failed_events"]:
                h_hardware.Fill(one_muon)
        m_max = 0
        for ibin in range(h_frame.GetNbinsX()):
            if h_frame.GetBinContent(ibin+1) > m_max:
                m_max = h_frame.GetBinContent(ibin+1)
        h_frame.Draw("hist")
        if ishardware:
            h_hardware.Draw("hist,same")
        h_ratio = h_frame.Clone()
        fit_max = 20.15
        fit_min = 0.15
        if len(Fit_range) == 2:
            fit_max = Fit_range[1]
            fit_min = Fit_range[0]
        fit = TF1("muon_fit","[0]*exp(-(x-[1])/[2])+[3]",fit_min,fit_max)
        fit.SetParameters(100.,0.0,2.2,0)
        #fit.FixParameter(0,0)
        fit.SetParLimits(0,0,10000000.)
        fit.SetParLimits(3,0,100000.)
        f_res_ptr = h_frame.Fit(fit,"RLS")
        f_res = f_res_ptr.Get()
        #for i in range(f_res.NTotalParameters()):
        #    print ("{} : {} +- {}".format(f_res.ParName(i),f_res.Parameter(i),f_res.ParError(i)))
        #h_fit = h_ratio.Clone()
        #h_fit.Reset()
        #for ibin in range(h_frame.GetNbinsX()):
        #    a,a_err = CalForFit(h_frame.GetBinCenter(ibin+1),h_frame.GetBinWidth(ibin+1)/2.,
        #                        f_res.Parameter(0),f_res.ParError(0),
        #                        f_res.Parameter(1),f_res.ParError(1),
        #                        f_res.Parameter(2),f_res.ParError(2),
        #                        f_res.Parameter(3),f_res.ParError(3)
        #                         )
        #    h_fit.SetBinContent(ibin+1,a)
        #    h_fit.SetBinError(ibin+1,a_err)
        h_ratio.Divide(fit)
        m_latex = TLatex()
        m_latex.SetTextSize(0.05)
        fit.SetLineColor(2)
        fit.Draw("same")
        h_frame.GetYaxis().SetTitleSize(0.06)
        h_frame.GetYaxis().SetTitleOffset(1.0)
        h_frame.GetXaxis().SetTitleSize(0)
        h_frame.GetXaxis().SetLabelSize(0)
        h_frame.GetYaxis().SetLabelSize(0.06)
        #h_frame.GetXaxis().SetTitleSize(0.04)
        #h_frame.GetXaxis().SetTitleOffset(1.05)
        h_frame.GetYaxis().SetRangeUser(0.,2*m_max)
        self.canvas.cd()
        m_latex.DrawLatexNDC(0.15,0.94,"{} events of data.".format(len(Muon_list["Muon_events"])))
        text_pos = 0.82
        m_latex.DrawLatexNDC(0.20,text_pos,"#font[22]{TANAKA Lab}")
        if "Channel" in Muon_list:
                text_pos -= 0.07
                m_latex.DrawLatexNDC(0.20,text_pos,"Channel {}".format(Muon_list["Channel"]))
        text_pos -= 0.07
        m_latex.DrawLatexNDC(0.20,text_pos,"Fit to #font[12]{p0*e^{-(t-p1)/p2}+p3}")
        self.pad2.cd()
        h_ratio.Draw("pe")
        
        h_ratio.GetYaxis().SetRangeUser(0,2)
        h_ratio.GetXaxis().SetTitleSize(0.15)
        h_ratio.GetXaxis().SetLabelSize(0.15)
        h_ratio.GetYaxis().SetLabelSize(0.15)
        h_ratio.GetXaxis().SetTitleOffset(1.0)
        h_ratio.GetYaxis().SetTitleSize(0.15)
        h_ratio.GetYaxis().SetTitleOffset(0.4)
        h_ratio.GetYaxis().SetNdivisions(504)
        h_ratio.GetYaxis().SetTitle("Data/Fit")
        self.canvas.Modified()
        self.canvas.Update()
        self.canvas.Print(os.path.join(Output,self.name+".png"))

def DrawGR(data,peds,h_frame,m_gr,m_label,labelx, labely, Title_offset):
        m_latex = TLatex()
        m_latex.SetTextSize(0.05)
        line = TLine()
        line.SetLineColor(2)
        line.SetLineStyle(2)
        h_frame.Draw()
        if Title_offset:
            h_frame.GetYaxis().SetTitleOffset(Title_offset)
        if data != None:
            m_max = max(data)
            m_min = min(data)
            h_frame.GetYaxis().SetRangeUser(0 if m_min-2*(m_max-m_min)<0 else m_min-2*(m_max-m_min),m_max+2*(m_max-m_min))
	#if peds != None:
        #    line.DrawLine(x_min,peds,x_max,peds)
        if m_gr!= None:
            m_gr.Draw("L")
        m_latex.DrawLatexNDC(labelx,labely,m_label)

class PlotCoincidenceFive(PlotBase):
    def __init__(self, ADCs,flags, Output, fout,**kwargs):
        super(PlotCoincidenceFive, self).__init__(**kwargs)
        self.canvas.Divide(4,2);
        self.pad1 = self.canvas.cd(1)
        self.pad2 = self.canvas.cd(2)
        self.pad3 = self.canvas.cd(3)
        self.pad4 = self.canvas.cd(4)
        self.pad5 = self.canvas.cd(5)
        self.pad6 = self.canvas.cd(6)
        self.pad7 = self.canvas.cd(7)
        self.pad8 = self.canvas.cd(8)
        self.pad1.SetTopMargin(0.12)
        self.pad2.SetTopMargin(0.12)
        self.pad3.SetTopMargin(0.12)
        self.pad4.SetTopMargin(0.12)
        self.pad5.SetTopMargin(0.12)
        self.pad6.SetTopMargin(0.12)
        self.pad8.SetTopMargin(0.12)
        self.pad1.SetLeftMargin(0.17)
        self.pad2.SetLeftMargin(0.17)
        self.pad3.SetLeftMargin(0.17)
        self.pad5.SetLeftMargin(0.17)
        self.pad6.SetLeftMargin(0.17)
        x_max = 10000
        if self.x_max!= None:
            x_max = self.x_max
        x_min = 0
        if self.x_min!= None:
            x_min = self.x_min

        h_0 = TH1D(self.name+"_ch0",";Sampling points;ADC",10000,0,10000)
        h_1 = TH1D(self.name+"_ch1",";Sampling points;ADC",10000,0,10000)
        h_2 = TH1D(self.name+"_ch2",";Sampling points;ADC",10000,0,10000)
        h_3 = TH1D(self.name+"_ch3",";Sampling points;ADC",10000,0,10000)
        h_4 = TH1D(self.name+"_ch4",";Sampling points;ADC",10000,0,10000)
        h_ab_c = TH1D(self.name+"_ab_c",";Sampling points;Flags",10000,0,10000)
        h__ab_c = TH1D(self.name+"__ab_c",";Sampling points;Flags",10000,0,10000)
        self.set_x_axis_bounds(h_0)
        self.set_x_axis_bounds(h_1)
        self.set_x_axis_bounds(h_2)
        self.set_x_axis_bounds(h_3)
        self.set_x_axis_bounds(h_4)
        self.set_x_axis_bounds(h_ab_c)
        self.set_x_axis_bounds(h__ab_c)
        for ibin in range(h_ab_c.GetNbinsX()):
            h_0.SetBinContent(ibin+1,-10)
            h_1.SetBinContent(ibin+1,-10)
            h_2.SetBinContent(ibin+1,-10)
            h_3.SetBinContent(ibin+1,-10)
            h_4.SetBinContent(ibin+1,-10)
            h_ab_c.SetBinContent(ibin+1,-10)
            h__ab_c.SetBinContent(ibin+1,-10)
        gr_0 = TGraph(len(ADCs[0]),array.array('d',range(len(ADCs[0]))),array.array('d',ADCs[0]))
        gr_1 = TGraph(len(ADCs[1]),array.array('d',range(len(ADCs[1]))),array.array('d',ADCs[1]))
        gr_2 = TGraph(len(ADCs[2]),array.array('d',range(len(ADCs[2]))),array.array('d',ADCs[2]))
        gr_3 = None
        if 3 in ADCs:
            gr_3 = TGraph(len(ADCs[3]),array.array('d',range(len(ADCs[3]))),array.array('d',ADCs[3]))
        gr_4 = None
        if 4 in ADCs:
            gr_4 = TGraph(len(ADCs[4]),array.array('d',range(len(ADCs[4]))),array.array('d',ADCs[4]))
        gr_ab_c = TGraph(len(flags["AB_C"]),array.array('d',range(len(flags["AB_C"]))),array.array('d',flags["AB_C"]))
        gr__ab_c = TGraph(len(flags["_AB_C"]),array.array('d',range(len(flags["_AB_C"]))),array.array('d',flags["_AB_C"]))

        self.pad1.cd()
        DrawGR(ADCs[0],ADCs["p_0"] if "p_0" in ADCs else None, h_0, gr_0, "Channel {0}".format("A"), 0.20, 0.73, 1.9)
        self.pad2.cd()
        DrawGR(ADCs[1],ADCs["p_1"] if "p_1" in ADCs else None, h_1, gr_1, "Channel {0}".format("B"), 0.20, 0.73, 1.9)
        self.pad3.cd()
        DrawGR(ADCs[2],ADCs["p_2"] if "p_2" in ADCs else None, h_2, gr_2, "Channel {0}".format("C"), 0.20, 0.73, 1.9)
        self.pad5.cd()
        if 3 in ADCs:
            DrawGR(ADCs[3],ADCs["p_3"] if "p_3" in ADCs else None, h_3, gr_3, "{0}".format("HW START"), 0.20, 0.73, 1.9)
        else:
            DrawGR(None,None, h_3, gr_3, "Channel {0}".format(3), 0.20, 0.73, 1.9)
        self.pad6.cd()
        if 4 in ADCs:
            DrawGR(ADCs[4],ADCs["p_4"] if "p_4" in ADCs else None, h_4, gr_4, "{0}".format("HW STOP"), 0.20, 0.73, 1.9)
        else:
            DrawGR(None,None, h_4, gr_4, "Channel {0}".format(3), 0.20, 0.73, 1.9)
        self.pad4.cd()
        DrawGR(flags["AB_C"],None, h_ab_c, gr_ab_c,"AB#bar{C}", 0.20, 0.73, None)
        h_ab_c.GetYaxis().SetRangeUser(-1,2)
        self.pad8.cd()
        DrawGR(flags["_AB_C"],None, h__ab_c, gr__ab_c,"#bar{A}B#bar{C}", 0.20, 0.73, None)
        h__ab_c.GetYaxis().SetRangeUser(-1,2)
        self.canvas.cd()

        self.pad7.cd()
        m_latex = TLatex()
        m_latex.SetTextSize(self._setNewTextSizeOnPad(0.05,self.pad7))
        if ADCs["Pass"] == 1:
            m_latex.DrawLatexNDC(self._setNewCoordinateOnPad(0.60,self.pad7),self._setNewCoordinateOnPad(0.1,self.pad7,"y"),"{0}".format( "#color[3]{Passed}"))
        else:
            m_latex.DrawLatexNDC(self._setNewCoordinateOnPad(0.60,self.pad7),self._setNewCoordinateOnPad(0.1,self.pad7,"y"),"{0}".format( "#color[2]{Failed}"))
#        if fout != None:
#            self.pad1.cd()
#            m_latex.SetTextSize(self._setNewTextSizeOnPad(0.04,self.pad1))
#	
#            m_latex.DrawLatexNDC(self._setNewCoordinateOnPad(0.05,self.pad1),self._setNewCoordinateOnPad(0.96,self.pad1,"y"),"Project: {0}, File Name : {1}".format( ADCs["Project"],ADCs["FileName"]))
#            self.pad2.cd()
#            m_latex.SetTextSize(self._setNewTextSizeOnPad(0.04,self.pad2))
#	
#            m_latex.DrawLatexNDC(self._setNewCoordinateOnPad(0.05,self.pad2),self._setNewCoordinateOnPad(0.96,self.pad2,"y"),"Project: {0}, File Name : {1}".format( ADCs["Project"],ADCs["FileName"]))
        self.canvas.cd()
        m_latex.SetTextSize(0.04)
        m_latex.DrawLatexNDC(0.05,0.96,"Project: {0}, File Name : {1}".format( ADCs["Project"],ADCs["FileName"]))

        self.canvas.Modified()
        self.canvas.Update()
        if fout != None:
            fout.cd()
            self.canvas.Write(self.name)
        else:
            self.canvas.Print(os.path.join(Output,self.name+".png"))

class PlotPlain(PlotBase):
    def __init__(self, data, Output, **kwargs):
        super(PlotPlain, self).__init__(**kwargs)
        gStyle.SetOptStat(1111)

        h = TH1D("h_"+self.name,";"+self.x_title+";"+self.y_title,1000,-1000,1000)
        for x in data:
            h.Fill(x)
        self.set_x_axis_bounds(h)
        h.Draw("hist")
        h.SetStats(True)
        self.canvas.Modified()
        self.canvas.Update()
        self.canvas.Print(os.path.join(Output,self.name+".png"))
        gStyle.SetOptStat(0)

class PlotAChannel(PlotBase):
    def __init__(self, Histo, Output, **kwargs):
        super(PlotAChannel, self).__init__(**kwargs)

        self.canvas.SetLeftMargin(0.18)
        self.canvas.SetTopMargin(0.11)
        self.canvas.SetBottomMargin(0.13)

        h_adc = TH1D(self.name,";"+self.x_title+";"+self.y_title,10000,-0.5,9999.5)
        h_adc.SetStats(0)
        self.set_y_min(h_adc)
        self.set_y_max(h_adc)
        self.set_x_axis_bounds(h_adc)
        m_latex = TLatex()
        m_latex.SetTextSize(0.05)
        h_adc.Draw("hist")
        #h_adc.GetYaxis().SetRangeUser(,10000)
        h_adc.GetYaxis().SetTitleSize(0.05)
        h_adc.GetYaxis().SetTitleOffset(1.2)
        h_adc.GetXaxis().SetTitleSize(0.04)
        h_adc.GetXaxis().SetTitleOffset(1.05)

        for gr in Histo["ADC"]: 
            gr.Draw("L")
        m_latex.DrawLatexNDC(0.18,0.92,"{} events of data. Max ADC : {}. Min ADC : {}".format(Histo["nEvents"],Histo["Max"],Histo["Min"]))
        m_latex.DrawLatexNDC(0.25,0.80,"Channel {}".format(Histo["Channel"]))
        self.canvas.Modified()
        self.canvas.Update()
        self.canvas.Print(os.path.join(Output,self.name+".png"))
