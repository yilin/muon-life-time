#!/usr/bin/env python
from argparse import ArgumentParser
from ROOT import TH1D,TCanvas,gROOT,TGraph,TLatex,gStyle,TF1,gDirectory,TPad,TLine,TFile
import array
gROOT.SetBatch(True)
gROOT.Reset()

print("Loading scripts (This will take some time)")
gROOT.LoadMacro('./script/Muon_raw.C') # load class Muon_raw in ROOT

from ROOT import Muon_raw # import class Muon_raw from ROOT

import math
import sys,os
sys.path.append('lib/')

import atlas_style
from plotter import *

import time,datetime

def ReadInput(isList,m_file):
    if not isList: return [["",m_file]]
    files = []
    with open(List,'r') as fp:
        m_path = ""
        for line in fp.readlines():
            if "#" in line : continue
            line = line.replace('\n','').replace('\t','')
            line_token = line.split()
            if len(line_token)>0 and line_token[0] == "Path":
                m_path = line_token[2]
            else:
                files.append([m_path,line.replace(' ','')])
    return files

def Run_basic(Input,List,Output,Life_time):
    # Read a root file or a list of root files
    islist = os.path.isfile(List)
    files =  []
    if islist:
        files = ReadInput(islist, List)
    else:
        files = ReadInput(islist,Input)

    channels = ["0","1","2","3","4","5","6","7"]
    Histo = {}
    Muon_list = {}
    Muon_list["Muon_events"] = []
    #loop all files 
    for full_path in  files:
        full_name = os.path.join(full_path[0],full_path[1])
        m_r = Muon_raw(full_name) # see ./script/Muon_raw.h for details
        if Life_time:
            m_r.Lifetime()
        else:
            m_r.AllChannelPlots()
        m_r.Loop()
        if Life_time:
            Muon_list["Channel"] = m_r.channel_stop
            for n in range(m_r.muon_lifetimes.size()):
                Muon_list["Muon_events"].append(m_r.muon_lifetimes[n])
        else:
            for ch in channels:
                if m_r.grs.find(ch)!=m_r.grs.end():
                    if ch not in Histo:
                        Histo[ch] = {}
                        Histo[ch]["ADC"] = []
                        Histo[ch]["Max_list"] = []
                        Histo[ch]["Min_list"] = []
                        Histo[ch]["Channel"] = ch
                        Histo[ch]["nEvents"] = 0
                    for n_gr in range(m_r.grs[ch].size()):
                        Histo[ch]["ADC"].append(m_r.grs[ch][n_gr])
                    Histo[ch]["Max_list"].append(m_r.max_mins[ch].first)
                    Histo[ch]["Min_list"].append(m_r.max_mins[ch].second)
                    Histo[ch]["nEvents"] += m_r.fChain.GetEntries()
                else:
                    print("Channel {0} is not found in {1}!".format(ch,full_name))

    if Life_time:
        PlotMuonAndFit(
                     Muon_list,
                     False,
                     Output,
                     #Fit_range = [1,20.15],
                     Fit_range = [0.15,20.15],
                     name = "Muon_life_time_012",
                     Label_Off = True,
                     x_title = "Decay time [#mus]",
                     y_title = "Events",
                     #y_min = -100,
                     #y_max = 100,
                     #y_min = Histo["Bias_Min"]-2*(Histo["Bias_Max"]-Histo["Bias_Min"]),
                     #y_max = Histo["Bias_Max"]+2*(Histo["Bias_Max"]-Histo["Bias_Min"]),
                     width = 1000,
                     )
    else:
        for ch in channels:
            if ch in Histo:
                Histo[ch]["Max"] = max(Histo[ch]["Max_list"])
                Histo[ch]["Min"] = max(Histo[ch]["Min_list"])
                PlotAChannel(
                                Histo[ch],
                                Output,
                                name = "my_adc_"+ch,
                                Label_Off = True,
                                x_title = "Sampling points",
                                y_title = "ADC",
                                y_min = 0 if Histo[ch]["Min"]-2*(Histo[ch]["Max"]-Histo[ch]["Min"])<0 else Histo[ch]["Min"]-2*(Histo[ch]["Max"]-Histo[ch]["Min"]),
                                y_max = Histo[ch]["Max"]+2*(Histo[ch]["Max"]-Histo[ch]["Min"]),
                                x_min = 0,
                                x_max = 200,
                                #y_min = 0 ,
                                #y_max = 16000,
                                width = 1000,
                                )


if __name__=="__main__":

    parser = ArgumentParser()
    parser.add_argument("-i", dest='Input',default="FADC.txt",help="The file of ADC output")
    parser.add_argument("-l", dest='List',default="",help="The list for the files of ADC output")
    parser.add_argument("--life-time", dest='Life_time',action="store_true",help="Peak up the event with muon decay")
    parser.add_argument("-o", dest='Output',default="./",help="The output directory")

    args = parser.parse_args()
    Input = args.Input
    List = args.List
    Output = args.Output
    Life_time = args.Life_time

    stamp_a = datetime.timedelta(seconds=time.time())

    if not os.path.isdir(Output):
        os.makedirs(Output)

    Run_basic(Input,List,Output,Life_time)

    stamp_b = datetime.timedelta(seconds=time.time())
    print("Process time : {0}".format(stamp_b-stamp_a))
