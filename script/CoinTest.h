#ifndef __COINTEST_H__
#define __COINTEST_H__
class CoinTest{
	public:
		CoinTest(std::string root_file, std::string peds_path = "../pedestal.txt");

		~CoinTest();

		// delay for shift pulses for coincidence analysis
		void setDelay(int32_t m_delay) {delay = m_delay;return;}

		// threshold for identify the pulses
		void setSlope(int32_t m_slope) {slope = m_slope;return;}
		
		Long64_t GetEntries(){return t->GetEntries();}

		// Process coincidence analysis and identify the hardware signals
		void ProcessEntry(Long64_t entry); 
	
		//
                // retrieve infos for a events
		//
		bool isPass(){return ispass;} // found muon or not
		bool isHWPass(){return ishwpass;} // found muon with hardware coincidence
		double GetLife_time(){return life_time;} // Get muon life time by coincidence analysis
		bool HasSTART_comp(){return isstart_hwcomp;} // found START signal from hardware coincidence
		double GetSTART_comp(){return start_hwcomp;} // get the START point from hardware coincidence
		bool HasSTOP_comp(){return isstop_hwcomp;} // found STOP signal from hardware coincidence
		double GetSTOP_comp(){return stop_hwcomp;} // get the STOP point from hardware coincidence

		// get project name, usually the root file names
		const char *GetProject(){return Project->c_str();}
		// get name of Run_*.log for this event
		const char *GetFileName(){return FileName->c_str();}

		// Get number of sampling points
		size_t GetASize(){return adc_a->size();}
		size_t GetBSize(){return adc_b->size();}
		size_t GetCSize(){return adc_c->size();}
		size_t GetSTARTSize(){return adc_start->size();}
		size_t GetSTOPSize(){return adc_stop->size();}

		// Get channel mapping ex: scintillator A -> ADC channel 0
		size_t GetChannelA(){return channel_a;}
		size_t GetChannelB(){return channel_b;}
		size_t GetChannelC(){return channel_c;}
		size_t GetChannelSTART(){return channel_start;}
		size_t GetChannelSTOP(){return channel_stop;}

		//retrieve vectors of all sampling points for each events
		std::vector<int>     GetA(){if (adc_a)return *adc_a; return {};}
		std::vector<int>     GetB(){if (adc_b) return *adc_b; return {};}
		std::vector<int>     GetC(){if (adc_c) return *adc_c;return {};}
		std::vector<int>     GetSTART(){if (adc_start) return *adc_start; return {};}
		std::vector<int>     GetSTOP(){if (adc_stop)return *adc_stop; return {};}
	public:
		std::vector<double> muon_list; // vector of muon life time from coincidence analysis
		std::vector<double> muon_list_hw_failed;// vector of muon life time from hardware coincidence
		std::vector<double> start_comp; // differences of START signals between coincidence analysis and hardware coincidence
		std::vector<double> stop_comp; // differences of START signals between coincidence analysis and hardware coincidence
		std::vector<int32_t> flags_AB_C; // START signals from coincidence analysis
		std::vector<int32_t> flags__AB_C; // STOP signals from coincidence analysis
	private:
		void ResetVars(); // Reset variables for each loop
	
		void ReadPeds(std::string peds_path); // Read the pedestal level

		bool START(double a,double b,double c,int32_t  &slope);  // identify the START logic for each set of sampling points

		bool STOP(double a, double b, double c, int32_t &slope); // identify the STOP logic for each set of sampling points

		std::vector<std::vector<int32_t>> ScanFlags(std::vector<int> &flag_list); // identify where START or STOP signals start or stop

		std::vector<int32_t> HardwareSTARTSTOP(std::vector<int> *start_data, double &start_ped, std::vector<int> *stop_data, double &stop_ped, int32_t &slope); // identify where START or STOP signals start from hardware coincidence

		std::vector<int>* distributor(int32_t channel); // map ADC channels to START or STOP channels ex: ADC channel 0 -> STOP channel

	private:
		TFile *f;
		TTree *t;
		std::string          *Project;
		std::string          *FileName;
		Int_t           channel_a;
		Int_t           channel_b;
		Int_t           channel_c;
		Int_t           channel_start;
		Int_t           channel_stop;
		Int_t           event;
		std::vector<int>     *adc_0;
		std::vector<int>     *adc_1;
		std::vector<int>     *adc_2;
		std::vector<int>     *adc_3;
		std::vector<int>     *adc_4;
		std::vector<int>     *adc_5;
		std::vector<int>     *adc_6;
		std::vector<int>     *adc_7;

		std::vector<int>     *adc_a;
		std::vector<int>     *adc_b;
		std::vector<int>     *adc_c;
		std::vector<int>     *adc_start;
		std::vector<int>     *adc_stop;
		std::map<std::string,double> peds;
		int32_t delay;
		int32_t slope;
		bool ispass;
		bool ishwpass;
		double life_time;
		bool isstart_hwcomp;
		double start_hwcomp;
		bool isstop_hwcomp;
		double stop_hwcomp;
};

#endif
