#ifndef __MYREPROCESS_H__
#define __MYREPROCESS_H__

// Showing the progress status when reading the Run_*.log  files
// define the mutex to prevent reading and writing at same time
std::mutex rMutex1;
std::mutex rMutex2;
std::mutex rMutex3;
std::mutex rMutex4;

uint64_t FILE_REPROCESS_TOTAL_LINE = 0;
uint64_t FILE_REPROCESS_DATA_LINE = 0;
bool FILE_REPROCESSING = false;
std::string FILE_REPROCESS_SHOW = "";

void FILE_REPROCESS_READ_TOTAL_LINE(uint64_t &a){
  std::lock_guard<std::mutex> mLock( rMutex1 );
  a = FILE_REPROCESS_TOTAL_LINE;
  return;
}
void FILE_REPROCESS_WRITE_TOTAL_LINE(uint64_t &a){
  std::lock_guard<std::mutex> mLock( rMutex1 );
  FILE_REPROCESS_TOTAL_LINE = a;
  return;
}
void FILE_REPROCESS_READ_DATA_LINE(uint64_t &a){
  std::lock_guard<std::mutex> mLock( rMutex2 );
  a = FILE_REPROCESS_DATA_LINE;
  return;
}
void FILE_REPROCESS_WRITE_DATA_LINE(uint64_t &a){
  std::lock_guard<std::mutex> mLock( rMutex2 );
  FILE_REPROCESS_DATA_LINE = a;
  return;
}

void FILE_REPROCESS_READ_SHOW(std::string &a){
  std::lock_guard<std::mutex> mLock( rMutex3 );
  a = FILE_REPROCESS_SHOW;
  return;
}
void FILE_REPROCESS_WRITE_SHOW(std::string &a){
  std::lock_guard<std::mutex> mLock( rMutex3 );
  if(a.size()!=0)FILE_REPROCESS_SHOW = a;
  return;
}

void FILE_REPROCESS_READ_FINISH(bool  &a){
  std::lock_guard<std::mutex> mLock( rMutex4 );
  a = FILE_REPROCESSING;
  return;
}
void FILE_REPROCESS_WRITE_FINISH(bool &a){
  std::lock_guard<std::mutex> mLock( rMutex4 );
  FILE_REPROCESSING = a;
  return;
}

// main loop for process status
void LoopFILE_REPROCESS(bool startprbar = true){
  
  time_t begintime = time(0);
  uint64_t total_line = 0;
  uint64_t data_line = 0;
  std::string showme = "";
  while(startprbar){
    usleep(100000);
    FILE_REPROCESS_READ_TOTAL_LINE(total_line);
    FILE_REPROCESS_READ_DATA_LINE(data_line);
    //printf("\r");
    printf("\r    Total Line: (%lu/\?\?\?), Data Line (%lu/\?\?\?) ", total_line, data_line); // show the read lines
    time_t timepassed = time(0) - begintime;
    time_t hh = timepassed / 3600;
    time_t mm = (timepassed % 3600) / 60;
    time_t ss = timepassed % 60;
    std::stringstream sos;
    sos << hh;
    string s_hh = sos.str(); sos.str(std::string());
    sos << mm;
    string s_mm = sos.str(); sos.str(std::string());
    sos << ss;
    string s_ss = sos.str(); sos.str(std::string());
    if (s_mm.size()==1) s_mm = "0" + s_mm;
    if (s_ss.size()==1) s_ss = "0" + s_ss;
    printf("%3s:%2s:%2s ",s_hh.c_str(),s_mm.c_str(),s_ss.c_str()); // show the process time
    FILE_REPROCESS_READ_SHOW(showme);
    if(showme.size()!=0)printf("%15s",showme.c_str());
    cout.flush();
    FILE_REPROCESS_READ_FINISH(startprbar);
    startprbar = !startprbar;
    if (!startprbar){ // process finished, show the final information
      FILE_REPROCESS_READ_TOTAL_LINE(total_line);
      FILE_REPROCESS_READ_DATA_LINE(data_line);
      FILE_REPROCESS_READ_SHOW(showme);
      printf("\r    Total Line: %lu, Data Line: %lu ", total_line, data_line); 
      printf("%3s:%2s:%2s ",s_hh.c_str(),s_mm.c_str(),s_ss.c_str());
      if(showme.size()!=0)printf("%15s",showme.c_str());
      printf("               ");
      cout.flush();
      printf("\n");
    }
  }
  total_line = 0;
  data_line = 0;
  startprbar = false;
  FILE_REPROCESS_WRITE_TOTAL_LINE(total_line);
  FILE_REPROCESS_WRITE_DATA_LINE(data_line);
  FILE_REPROCESS_WRITE_FINISH(startprbar);
  return;
}

// searching all Run_*.log
// May have a buggy behaviour for the name format of Run_*.log, where * is not the numbers
std::vector<std::string> SearchFiles(std::string directory){
	DIR *dp; 
	struct dirent *dirp; 
	std::vector<std::string> all_files;
	if ((dp=opendir(directory.c_str())) != NULL){  // open the directory
		while((dirp= readdir(dp))!= NULL){   // loop the files and directory
			//printf("d_name : %s\n",dirp->d_name); 
			std::string mfile = dirp->d_name;
			if (mfile.find(".log")!=std::string::npos){ // find *.log
				all_files.push_back(mfile); // add all found files
			}
		} 
        	closedir(dp);  // close the directory
	} 
	std::vector<std::string> all_files_sorted;
	int32_t counter = 0;
	uint32_t nsorted = 0;
	int32_t pre_found = 0;
	printf("Sorting files\n");
	while(nsorted<all_files.size()){ // loop all files with *.log
                if (counter-pre_found> 100000){
			printf("Failed to sort the files!");
			break;
		}
		stringstream ss;
		ss << counter;
		std::string file_name = "Run_" + ss.str() + ".log";
		if (std::find (all_files.begin(),all_files.end(),file_name)!=all_files.end()){ // select only files with Run_*.log
			all_files_sorted.push_back(file_name);
			nsorted++;
			pre_found = counter;
		}
		counter++;
	}
	
	return all_files_sorted; //return sorted file list
}

#endif
