//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Sat Jun 19 18:19:03 2021 by ROOT version 6.24/00
// from TTree Muon_raw/Muon_raw
// found on file: Run_0614_1_invertStartStop.root
//////////////////////////////////////////////////////////

#ifndef Muon_raw_h
#define Muon_raw_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
//#include <da-linux-gnu/include/c++/9.3.0/string>
//#include <da-linux-gnu/include/c++/9.3.0/vector>

class Muon_raw {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   string          *Project;
   string          *FileName;
   Int_t           event;
   Int_t           channel_a;
   Int_t           channel_b;
   Int_t           channel_c;
   Int_t           channel_start;
   Int_t           channel_stop;
   vector<int>     *adc_0;
   vector<int>     *adc_1;
   vector<int>     *adc_2;
   vector<int>     *adc_3;
   vector<int>     *adc_4;
   vector<int>     *adc_5;
   vector<int>     *adc_6;
   vector<int>     *adc_7;

   // List of branches
   TBranch        *b_Project;   //!
   TBranch        *b_FileName;   //!
   TBranch        *b_event;   //!
   TBranch        *b_channel_a;   //!
   TBranch        *b_channel_b;   //!
   TBranch        *b_channel_c;   //!
   TBranch        *b_channel_start;   //!
   TBranch        *b_channel_stop;   //!
   TBranch        *b_adc_0;   //!
   TBranch        *b_adc_1;   //!
   TBranch        *b_adc_2;   //!
   TBranch        *b_adc_3;   //!
   TBranch        *b_adc_4;   //!
   TBranch        *b_adc_5;   //!
   TBranch        *b_adc_6;   //!
   TBranch        *b_adc_7;   //!

   Muon_raw(std::string file_name);
   virtual ~Muon_raw();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop(); // Loop all the entries
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);

public:
   // retrieve infos for a event, not in use currently
   void ProcessAEntry(Long64_t entry);

   // set an option for plotting all sampling points 
   void AllChannelPlots(){isAllchplots=true;return;}

   // set an option for getting muon life time
   void Lifetime(){islifetime=true;return;}

   std::map<std::string, std::vector<TGraph*>> grs; //map<channels, TGraphs for all sampling points>
   std::map<std::string, std::pair<int32_t, int32_t>> max_mins;//map<channels, pair<ADC maximum, ADC minimum>> 
   std::map<std::string, Long64_t> nEvents; //number of processed events

   std::vector<double> muon_lifetimes; // vector of all muon life times
private:
   void FillGraph(std::vector<int> *m_a, std::string key,int32_t &m_max,int32_t &m_min); // Fill sampling points to a TGraph for each event

   std::vector<int32_t> Pulse_detection(std::vector<int> *m_a); // identify the pulses

   double TimeConverter(int32_t start, int32_t stop); // microsecond

   std::vector<int>* distributor(int32_t channel); // map ADC channels to START or STOP channels
private:
   bool isAllchplots;
   bool islifetime;
   std::vector<std::string> channels;
   std::vector<std::vector<int>*> adcs;
};

#endif

#ifdef Muon_raw_cxx
Muon_raw::Muon_raw(std::string file_name) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   TFile *f = new TFile(file_name.c_str());
   TTree *tree = 0;
   f->GetObject("Muon_raw",tree);
   Init(tree);
   channels = {"0","1","2","3","4","5","6","7"};
}

Muon_raw::~Muon_raw()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t Muon_raw::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t Muon_raw::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void Muon_raw::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   Project = 0;
   FileName = 0;
   adc_0 = 0;
   adc_1 = 0;
   adc_2 = 0;
   adc_3 = 0;
   adc_4 = 0;
   adc_5 = 0;
   adc_6 = 0;
   adc_7 = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("Project", &Project, &b_Project);
   fChain->SetBranchAddress("FileName", &FileName, &b_FileName);
   fChain->SetBranchAddress("event", &event, &b_event);
   fChain->SetBranchAddress("channel_a", &channel_a, &b_channel_a);
   fChain->SetBranchAddress("channel_b", &channel_b, &b_channel_b);
   fChain->SetBranchAddress("channel_c", &channel_c, &b_channel_c);
   fChain->SetBranchAddress("channel_start", &channel_start, &b_channel_start);
   fChain->SetBranchAddress("channel_stop", &channel_stop, &b_channel_stop);
   fChain->SetBranchAddress("adc_0", &adc_0, &b_adc_0);
   fChain->SetBranchAddress("adc_1", &adc_1, &b_adc_1);
   fChain->SetBranchAddress("adc_2", &adc_2, &b_adc_2);
   fChain->SetBranchAddress("adc_3", &adc_3, &b_adc_3);
   fChain->SetBranchAddress("adc_4", &adc_4, &b_adc_4);
   fChain->SetBranchAddress("adc_5", &adc_5, &b_adc_5);
   fChain->SetBranchAddress("adc_6", &adc_6, &b_adc_6);
   fChain->SetBranchAddress("adc_7", &adc_7, &b_adc_7);

   adcs.push_back(adc_0);
   adcs.push_back(adc_1);
   adcs.push_back(adc_2);
   adcs.push_back(adc_3);
   adcs.push_back(adc_4);
   adcs.push_back(adc_5);
   adcs.push_back(adc_6);
   adcs.push_back(adc_7);

   Notify();
}

Bool_t Muon_raw::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void Muon_raw::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t Muon_raw::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef Muon_raw_cxx
