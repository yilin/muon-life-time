// Before use, include <iostream> <sring> <sstream> <stdio.h> <time.h> <unistd.h>
#ifndef _PPROGRESSBAR_H_
#define _PPROGRESSBAR_H_
using namespace std;
class ProgressBar{
  public:
    ProgressBar();

    void Start(uint64_t entry, uint64_t entries);

    void End();

    void Time();

    std::string PrintColor(int32_t color);

  private:
    void settime(time_t& tick, time_t& h,time_t& m,time_t& s);

    template <typename T>
    std::string ntos(T number);

  private:
    bool startprbar;

    time_t begintime;
    
};

ProgressBar::ProgressBar():startprbar(false),begintime(0){
}

template <typename T>
std::string ProgressBar::ntos(T number){
  std::stringstream ss;
  ss << number;
  return ss.str();
}

void ProgressBar::Start(uint64_t entry, uint64_t entries){
  string bar = "";
  uint64_t percent = (uint64_t)((double)entry/(double)entries*100+0.2);
  printf("\r");
  int32_t colorcode = 0;
  for (uint64_t i = 0; i < percent/2;i++){
    if (i % 7 == 0){
      if (colorcode < 7)colorcode++;
      bar += PrintColor(colorcode);
    }
    bar += " ";
    if (i == percent/2 -1) bar += PrintColor(10);
  }
  for (uint64_t i = 0; i < 50-percent/2;i++){
    bar += " ";
  }
  printf("\r%s (%3lu%%) ",bar.c_str(),percent );
  Time();
  if (entry != entries){
    startprbar = true;
  }
  else startprbar = false;
  return;
}

void ProgressBar::End(){
  cout.flush();
  if (!startprbar)printf("\n");
  return;
}

void ProgressBar::Time(){
  if (!startprbar){
    begintime = time(0);
  }
  time_t timepassed = time(0) - begintime;
  time_t hh = 0;
  time_t mm = 0;
  time_t ss = 0;
  settime(timepassed,hh,mm,ss);
  string s_hh = ntos(hh);
  string s_mm = ntos(mm);
  string s_ss = ntos(ss);
  if (s_mm.size()==1) s_mm = "0" + s_mm;
  if (s_ss.size()==1) s_ss = "0" + s_ss;
  printf("%3s:%2s:%2s ",s_hh.c_str(),s_mm.c_str(),s_ss.c_str());
  return;
}

void ProgressBar::settime(time_t& whattime, time_t& h, time_t& m, time_t& s){
  h = whattime / 3600;
  m = (whattime % 3600) / 60;
  s = whattime % 60;
  return;
}

std::string ProgressBar::PrintColor(int32_t color){
  switch(color){
    case 1:
      return "\033[48;5;1m";
      break;
    case 2:
      return "\033[48;5;196m";
      break;
    case 3:
      return "\033[48;5;202m";
      break;
    case 4:
      return "\033[48;5;220m";
      break;
    case 5:
      return "\033[48;5;226m";
      break;
    case 6:
      return "\033[48;5;40m";
      break;
    case 7:
      return "\033[48;5;46m";
      break;
    case 8:
      return "\033[48;5;82m";
      break;
    case 9:
      return "\033[48;5;118m";
      break;
    case 10:
      return "\033[0m";
      break;
  }
  return "";
}

mutex gMutex1;
mutex gMutex2;
mutex gMutex3;

uint64_t PPROGRESS_BAR_ENTRY = 0;
uint64_t PPROGRESS_BAR_ENTRIES = 10;
std::string PPROGRESS_BAR_SHOW = "";

void PPROGRESS_BAR_READ_E(uint64_t &a){
  lock_guard<mutex> mLock( gMutex1 );
  a = PPROGRESS_BAR_ENTRY;
  return;
}
void PPROGRESS_BAR_WRITE_E(uint64_t &a){
  lock_guard<mutex> mLock( gMutex1 );
  PPROGRESS_BAR_ENTRY = a;
  return;
}
void PPROGRESS_BAR_READ_ES(uint64_t &a){
  lock_guard<mutex> mLock( gMutex2 );
  a = PPROGRESS_BAR_ENTRIES;
  return;
}
void PPROGRESS_BAR_WRITE_ES(uint64_t &a){
  lock_guard<mutex> mLock( gMutex2 );
  PPROGRESS_BAR_ENTRIES = a;
  return;
}

void PPROGRESS_BAR_READ_SHOW(std::string &a){
  lock_guard<mutex> mLock( gMutex3 );
  a = PPROGRESS_BAR_SHOW;
  return;
}
void PPROGRESS_BAR_WRITE_SHOW(std::string &a){
  lock_guard<mutex> mLock( gMutex3 );
  if(a.size()!=0)PPROGRESS_BAR_SHOW = a;
  return;
}

void PPROGRESS_BAR_ADDONE(){
  lock_guard<mutex> mLock( gMutex2 );
  PPROGRESS_BAR_ENTRIES++;
  return;
}

void LoopBar(){
  ProgressBar p;
  std::vector<std::string> colorbar;
  for (size_t i = 6 ; i < 10; i++){
    colorbar.push_back(p.PrintColor(i));
  }
  for (size_t i = 9 ; i > 5; i-- ){
    colorbar.push_back(p.PrintColor(i));
  }
  bool startprbar = true;
  vector<int64_t> pos;
  for (size_t i = 0; i < colorbar.size();i++){
    pos.push_back(i-colorbar.size());
  }
  time_t begintime = time(0);
  uint64_t e = 0;
  uint64_t tot = 0;
  std::string showme = "";
  while(startprbar){
    usleep(100000);
    string bar = p.PrintColor(6);
    PPROGRESS_BAR_READ_E(e);
    PPROGRESS_BAR_READ_ES(tot);
    uint64_t percent = (uint64_t)((double)e/(double)tot*100+0.2);
    printf("\r");
    int32_t colorcode = 0;
    bar += p.PrintColor(colorcode);
    for (uint64_t i = 0; i < percent/2;i++){
      for (size_t color = 0; color < pos.size();color++){
        if (pos[color] < 0 ) continue;
        if (i == pos[color]) bar += colorbar[color];
      }
      bar += " ";
    }
    bar += p.PrintColor(10);
    for (uint64_t i = 0; i < 50-percent/2;i++){
      bar += " ";
    }
    printf("\r%s (%3lu%%) ",bar.c_str(),percent );
    time_t timepassed = time(0) - begintime;
    time_t hh = timepassed / 3600;
    time_t mm = (timepassed % 3600) / 60;
    time_t ss = timepassed % 60;
    std::stringstream sos;
    sos << hh;
    string s_hh = sos.str(); sos.str(std::string());
    sos << mm;
    string s_mm = sos.str(); sos.str(std::string());
    sos << ss;
    string s_ss = sos.str(); sos.str(std::string());
    if (s_mm.size()==1) s_mm = "0" + s_mm;
    if (s_ss.size()==1) s_ss = "0" + s_ss;
    printf("%3s:%2s:%2s ",s_hh.c_str(),s_mm.c_str(),s_ss.c_str());
    if (e == tot){
      startprbar = false;
    }
    PPROGRESS_BAR_READ_SHOW(showme);
    if(showme.size()!=0)printf("%15s",showme.c_str());
    cout.flush();
    if (!startprbar)printf("\n");
    for (size_t color = 0; color < pos.size();color++){
      pos[color]++;
    }
    if (pos[0] > 50){
      for (size_t i = 0; i < colorbar.size();i++){
        pos[i] = i-colorbar.size();
      }
    }
  }
  e = 0;
  tot = 10;
  PPROGRESS_BAR_WRITE_E(e);
  PPROGRESS_BAR_WRITE_ES(tot);
  return;
}

#endif
