#include <vector>
#include <string>
#include <sstream>
#include <map>
#include <thread>
#include <dirent.h>
#include <unistd.h>
#include <stdio.h>
#include <time.h>

#include "MyReProcess.h" // include "SearchFiles"  

void CalPedestal(std::string directory,std::string project, std::string output_dir){
	auto all_files = SearchFiles(directory); // search all Run_*.log
	std::vector<TH1*> h_peds;
	for (int32_t ch = 0; ch < 8; ch++ ){  // define TH1D for 8 channels
		stringstream ss;
		ss << ch;
		std::string name = "ped_" + ss.str();
		h_peds.push_back(new TH1D(name.c_str(),"",1,0,16000));
	}
	// loop all Run_*.log
	for (int i = 0; i < all_files.size();i++){ 
		std::string &mfile = all_files[i];
		std::fstream fp;	
		fp.open((directory+"/"+mfile).c_str(),std::ios::in);
		if (!fp){
			printf("Failed to read : %s\n",mfile.c_str());
			return;
		}
		std::string line;
		// read each line
		while(std::getline(fp,line)){
			stringstream ss(line);
			std::string element;
			std::vector<std::string> each_line;
			int32_t event;
			int32_t channel;
			int32_t adc;
			// fill one data line in a vector
			while(std::getline(ss,element,' ')){
				if (element.size()!=0)each_line.push_back(element);	
			}
			//printf("%zu\n",each_line.size());
			// confirm that it is a data line
			if(each_line.size()==5 && strcmp(each_line[0].c_str(),"DATA")==0){
				event = atoi(each_line[1].c_str()); // convert a event string to integer
				channel = atoi(each_line[2].c_str()); // convert a channel string to integer
				adc = atoi(each_line[4].c_str()); // convert a adc reading string to integer
				h_peds[channel]->Fill(adc);
			}
		}
		fp.close(); // close file
	}
	// open an output file for saving the pedestal levels
	fstream fout;
	fout.open((output_dir+"/"+project+"_pedestal.txt").c_str(),std::ios::out);
	if (!fout){
		printf("Too bad! Failed to write : %s\n",(output_dir+"/"+project+"_pedestal.txt").c_str());
		return;
	}
	printf("Saving to %s\n",(output_dir+"/"+project+"_pedestal.txt").c_str());
	// writing the results
	for (int32_t ch = 0; ch < 8; ch++ ){
		fout << ch << ":" << h_peds[ch]->GetMean() << std::endl;
		printf("%d:%f\n",ch,h_peds[ch]->GetMean());
	}
	fout.close();
	return;

}
