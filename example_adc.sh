#!/bin/bash
echo "#########################################################################"
echo ""
echo "Converting text files into root file"
echo ""
echo "#########################################################################"
python reprocess.py -d data/example_adc/ -c config/channel_setting.config
echo "#########################################################################"
echo ""
echo "Plotting all channels"
echo ""
echo "#########################################################################"
python run_basic.py -i root_output/example_adc.root -o results/Run_example_adc_basic/
echo "#########################################################################"
echo ""
echo "Plotting the muon life time from START and STOP"
echo ""
echo "#########################################################################"
python run_basic.py -i root_output/example_adc.root -o results/Run_example_adc_basic/ --life-time
echo "#########################################################################"
echo ""
echo "Check correlation of A,B,C, START, and STOP"
echo ""
echo "#########################################################################"
python run_coin.py -i root_output/example_adc.root -o results/Run_example_adc_coin/
echo "#########################################################################"
echo ""
echo "Plotting the muon life time from A,B,C"
echo ""
echo "#########################################################################"
python run_coin.py -i root_output/example_adc.root -o results/Run_example_adc_coin/ --life-time
echo "#########################################################################"
echo ""
echo "Plotting the muon life time from A,B,C with correlation of START and STOP"
echo ""
echo "#########################################################################"
python run_coin.py -i root_output/example_adc.root -o results/Run_example_adc_coin_hardware/ --life-time --hardware
